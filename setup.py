import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()


requires = [
  'Chameleon==2.14',
  'Mako==0.9.0',
  'MarkupSafe==0.18',
  'PasteDeploy==1.5.0',
  'Pygments==1.6',
  'SQLAlchemy==0.8.4',
  'WebOb==1.3.1',
  'WebTest==2.0.10',
  'Werkzeug==0.9.4',
  'beautifulsoup4==4.3.2',
  'factory-boy==2.2.1',
  'pyramid==1.5a3',
  'pyramid-chameleon==0.1',
  'pyramid-debugtoolbar==1.0.9',
  'pyramid-mako==0.3.1',
  'pyramid-tm==0.7',
  'repoze.lru==0.6',
  'six==1.4.1',
  'transaction==1.4.1',
  'translationstring==1.1',
  'venusian==1.0a8',
  'waitress==0.8.8',
  'wsgiref==0.1.2',
  'zope.deprecation==4.0.2',
  'zope.interface==4.0.5',
  'zope.sqlalchemy==0.7.3',
]


setup(name='pcasample',
      version='0.0.1',
      description='pcasample',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='Lucas Taylor',
      author_email='ltaylor.volks@gmail.com',
      url='http://bitbucket.org/ltvolks/pcasample',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='pcasample',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = pcasample:main
      [console_scripts]
      init_sample_db = pcasample.scripts.initializedb:main
      """,
      )
