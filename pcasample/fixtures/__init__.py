from itertools import cycle
import random
import calendar
from datetime import datetime
from time import time

import factory
from factory.alchemy import SQLAlchemyModelFactory

from ..models import DBSession, Sale, Store

months = cycle(range(1, 13))



class StoreFactory(SQLAlchemyModelFactory):
    FACTORY_FOR = Store
    FACTORY_SESSION = DBSession

    name = factory.Sequence(lambda n: u'Express Store #{0}-{1:f}'.format(n, time()))
    taxrate = 0.076



class SaleFactory(SQLAlchemyModelFactory):
    FACTORY_FOR = Sale
    FACTORY_SESSION = DBSession
    FACTORY_HIDDEN_ARGS = ('year',)

    store = factory.SubFactory(StoreFactory)
    updated_at = factory.LazyAttribute(lambda o: o.created_at)
    checknum = factory.Sequence(lambda n: n)
    subtotal = factory.LazyAttribute(lambda o: random.uniform(1.0, 150.0))
    tax = factory.LazyAttribute(lambda o: o.store.taxrate * o.subtotal)
    gratuity = factory.LazyAttribute(lambda o: 0.18 * o.subtotal)
    total = factory.LazyAttribute(lambda o: o.subtotal + o.tax + o.gratuity)

    @factory.lazy_attribute
    def created_at(self):
        """
        Return a limited 'pseudo-random' datetime using a specific year and 
        cycling through the months and randomizing the days.
        2000-1-13
        2000-2-28
        2000-3-04
        2000 ...
        """
        month = months.next()
        return datetime(self.year,
                        month,
                        int(random.uniform(1, calendar.mdays[month])))

