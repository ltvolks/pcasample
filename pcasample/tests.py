from datetime import datetime
import unittest
import transaction

from pyramid import testing
try:
    from webtest import TestApp
except ImportError as e:
    TestApp = None

from .models import DBSession, Sale, Store
from .fixtures import SaleFactory, StoreFactory


def _initTestingDB(load_fixtures=True):
    """
    Initialize an in-memory sqlite database and load with sample data.
    Each month in the year 2000 will contain 2 sales records with random
    totals.
    
    :param load_fixtures: Boolean, if True generate sample data using
                          factories
    :returns: sqlalchemy `scoped_session`
    """
    from sqlalchemy import create_engine
    engine = create_engine('sqlite://')
    from .models import Base
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)
    if load_fixtures:
        with transaction.manager:
            store = StoreFactory()
            for i in range(24):
                SaleFactory(store=store, year=2000)
    return DBSession



class ViewTests(unittest.TestCase):
    def setUp(self):
        self.session = _initTestingDB(load_fixtures=False)
        self.config = testing.setUp()

    def tearDown(self):
        self.session.remove()
        testing.tearDown()


    def test_year_view(self):
        """
        Year view returns a context containing the requested year if 
        matched by that route, or default to the current year
        """
        from .views import index
        request = testing.DummyRequest()
        ctx = index(request)

        # Defaults to current year
        self.assertEqual(ctx['year'], datetime.now().year)
        self.assertEqual(ctx['project'], 'pcasample')
        
        # Matched years should be passed through
        request.matchdict['year'] = 2000
        ctx = index(request)
        self.assertEqual(ctx['year'], 2000)


    def test_sales_view(self):
        """
        Sales view should return a dictionary containing label and data
        keys for a year specified as a query parameter.
        """
        from .views import sales
        request = testing.DummyRequest()

        ctx = sales(request)
        self.assertItemsEqual(('label', 'data'), ctx.keys())
        
        # No data exists yet
        self.assertEqual(len(ctx['data']), 0, msg='An empty data list should be returned for the current year')

        # Add 2 sales entries for each month in 2000 (factory cycles each month)
        year = 2000
        with transaction.manager:
            store = StoreFactory()
            for i in range(24):
                SaleFactory(store=store, subtotal=90.00, tax=10.00,
                            gratuity=0.0, total=100.00, year=year)

        request.params['year'] = year
        ctx = sales(request)
        self.assertEqual(len(ctx['data']), 12, msg='Each month should contain test sales data')
        for i in range(12):
            self.assertEqual(ctx['data'][i][1], 200.0, msg="Each monthly total should equal $200.00")



@unittest.skipUnless(TestApp, "webtest must be installed to run these tests")
class FunctionalTests(unittest.TestCase):
    """
    Lightweight functional tests using WebTest
    """
    def setUp(self):
        from pcasample import main
        settings = { 'sqlalchemy.url': 'sqlite://'}
        app = main({}, **settings)
        self.testapp = TestApp(app)
        _initTestingDB()

    def tearDown(self):
        del self.testapp
        from pcasample.models import DBSession
        DBSession.remove()


    def test_404(self):
        self.testapp.get('/NotAThing', status=404)


    def test_jsonEndpoint(self):
        """
        /sales route should return a json object containing label and
        data keys
        """
        res = self.testapp.get('/xhr/sales?year=3000')
        self.assertEqual(res.json, {'data': [], 'label': '3000 Monthly Sales'})
        
        res = self.testapp.get('/xhr/sales?year=2000')
        self.assertEqual(res.json['label'], '2000 Monthly Sales')
        self.assertEqual(len(res.json['data']), 12)
