************************
Sample App for PCAmerica
************************

Setup
=====

1. Create a new virtualenv
2. pip install -e git+http://bitbucket.org/ltvolks/pcasample#egg=pcasample

All dependencies should install automatically


Run the app
===========

.. code:: bash

    $ cd $VENV/src/pcasample
    $ $VENV/bin/init_sample_db development.ini
    $ $VENV/bin/pserve development.ini
    $ open http://localhost:6543


Run the tests
=============

.. code:: bash
    
    $ $VENV/bin/python -m unittest -v pcasample.tests