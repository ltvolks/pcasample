from datetime import datetime

from sqlalchemy import (
    Column, ForeignKey, Index,
    DateTime, Float, Integer, String
    )
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import (
    scoped_session, sessionmaker, relationship
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))



class BaseMixin(object):
    """
    Customization point for defining our own mixin behavior
    Define some boilerplate columns that every model should have and
    convenience functions

    Models that use this mixin get:
        timestamped create/update columns
        `update` method for updating instances via kwargs

    #TODO: add JSON serialization
    """
    created_at = Column(DateTime, default=datetime.utcnow(), nullable=False)
    updated_at = Column(DateTime, onupdate=datetime.utcnow())

    def __repr__(self):
        return "<{0}: {1}>".format(self.__class__.__name__, getattr(self, 'id', id(self)))

Base = declarative_base(cls=BaseMixin)



class Store(Base):
    """
    Store/Restaurant definition
    """
    __tablename__ = 'store'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    taxrate = Column(Float)
    sales = relationship('Sale', backref='store')
    
    def __repr__(self):
        return "<Store: {0}>".format(self.name or 'NEW')
Index('store_name', Store.name, unique=True)



class Sale(Base):
    """
    Sale/Check entries
    """
    __tablename__ = 'sale'
    
    id = Column(Integer, primary_key=True)
    store_id = Column(ForeignKey('store.id'))
    checknum = Column(Integer, autoincrement=True, unique=True)
    subtotal = Column(Float)
    tax = Column(Float)
    gratuity = Column(Float)
    total = Column(Float)
Index('Sale_checknum', Sale.checknum, unique=True)