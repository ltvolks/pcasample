from datetime import datetime

from pyramid.view import view_config

from sqlalchemy import func, extract
from sqlalchemy.exc import OperationalError

from .models import  DBSession, Sale


@view_config(route_name='year', renderer='templates/index.pt')
@view_config(route_name='home', renderer='templates/index.pt')
def index(request):
    year = request.matchdict.get('year', datetime.now().year)
    return {'project': 'pcasample', 'year': year}


@view_config(route_name='sales', renderer='json')
def sales(request):
    """
    Returns sales data grouped by month.
    """
    current_year = datetime.now().year
    try:
        year = int(request.params.get('year', current_year))
    except ValueError as e:
        year = current_year

    month_crit = extract('month', Sale.created_at)
    year_crit = extract('year', Sale.created_at)
    series = DBSession.query(month_crit.label('month') , func.sum(Sale.total)).filter(year_crit==year).group_by('month').all()

    return {
        'label': '{0} Monthly Sales'.format(year),
        'data': series
    }
